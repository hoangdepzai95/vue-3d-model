import * as THREE from 'three';
import Vector3 from './Vector3';

// https://github.com/mrdoob/three.js/blob/dev/examples/js/loaders/STLLoader.js

const STLLoader = function ( manager ) {

    this.manager = ( manager !== undefined ) ? manager : THREE.DefaultLoadingManager;

    // from https://github.com/johannesboyne/node-stl/blob/master/index.js
    this.density = 1;
    this.volume = 0;
    this.area = 0;
    this.minx = Infinity;
    this.maxx = -Infinity;
    this.miny = Infinity;
    this.maxy = -Infinity;
    this.minz = Infinity;
    this.maxz = -Infinity;
    this.xCenter = 0;
    this.yCenter = 0;
    this.zCenter = 0;
    this.measuredData = {};

};

STLLoader.prototype = {

    constructor: STLLoader,

    load: function ( url, onLoad, onProgress, onError, onMeasure ) {

        var scope = this;

        var loader = new THREE.FileLoader( scope.manager );
        loader.setResponseType( 'arraybuffer' );
        loader.load( url, function ( text ) {

            onLoad( scope.parse( text ) );

            onMeasure(scope.measuredData);

        }, onProgress, onError );

    },

    parse: function ( data ) {

        var isBinary =  () => {

            var expect, face_size, n_faces, reader;
            reader = new DataView( binData );
            face_size = ( 32 / 8 * 3 ) + ( ( 32 / 8 * 3 ) * 3 ) + ( 16 / 8 );

            n_faces = reader.getUint32( 80, true );
            expect = 80 + ( 32 / 8 ) + ( n_faces * face_size );

            if ( expect === reader.byteLength ) {

                return true;

            }

            // An ASCII STL data must begin with 'solid ' as the first six bytes.
            // However, ASCII STLs lacking the SPACE after the 'd' are known to be
            // plentiful.  So, check the first 5 bytes for 'solid'.

            // US-ASCII ordinal values for 's', 'o', 'l', 'i', 'd'
            var solid = [ 115, 111, 108, 105, 100 ];

            for ( var i = 0; i < 5; i++ ) {

                // If solid[ i ] does not match the i-th byte, then it is not an
                // ASCII STL; hence, it is binary and return true.

                if ( solid[ i ] != reader.getUint8( i, false ) ) return true;

             }

            // First 5 bytes read "solid"; declare it to be an ASCII STL
            return false;

        };

        var binData = this.ensureBinary( data );

        return isBinary() ? this.parseBinary( binData ) : this.parseASCII( this.ensureString( data ) );

    },

    // from https://github.com/johannesboyne/node-stl/blob/master/index.js
    addTriangle(triangle) {
        let currentVolume = this._triangleVolume(triangle);
        this.volume += currentVolume;

        const ab = triangle[1].clone().sub(triangle[0]);
        const ac = triangle[2].clone().sub(triangle[0]);

        this.area +=
            ab
                .clone()
                .cross(ac)
                .length() / 2;

        const tminx = Math.min(triangle[0].x, triangle[1].x, triangle[2].x);
        this.minx = tminx < this.minx ? tminx : this.minx;
        const tmaxx = Math.max(triangle[0].x, triangle[1].x, triangle[2].x);
        this.maxx = tmaxx > this.maxx ? tmaxx : this.maxx;

        const tminy = Math.min(triangle[0].y, triangle[1].y, triangle[2].y);
        this.miny = tminy < this.miny ? tminy : this.miny;
        const tmaxy = Math.max(triangle[0].y, triangle[1].y, triangle[2].y);
        this.maxy = tmaxy > this.maxy ? tmaxy : this.maxy;

        const tminz = Math.min(triangle[0].z, triangle[1].z, triangle[2].z);
        this.minz = tminz < this.minz ? tminz : this.minz;
        const tmaxz = Math.max(triangle[0].z, triangle[1].z, triangle[2].z);
        this.maxz = tmaxz > this.maxz ? tmaxz : this.maxz;

        // Center of Mass calculation
        // adapted from c++ at: https://stackoverflow.com/a/2085502/6482703
        this.xCenter +=
            ((triangle[0].x + triangle[1].x + triangle[2].x) / 4) * currentVolume;
        this.yCenter +=
            ((triangle[0].y + triangle[1].y + triangle[2].y) / 4) * currentVolume;
        this.zCenter +=
            ((triangle[0].z + triangle[1].z + triangle[2].z) / 4) * currentVolume;
    },

    // from https://github.com/johannesboyne/node-stl/blob/master/index.js
    _triangleVolume(triangle) {
        const v321 = triangle[2].x * triangle[1].y * triangle[0].z,
            v231 = triangle[1].x * triangle[2].y * triangle[0].z,
            v312 = triangle[2].x * triangle[0].y * triangle[1].z,
            v132 = triangle[0].x * triangle[2].y * triangle[1].z,
            v213 = triangle[1].x * triangle[0].y * triangle[2].z,
            v123 = triangle[0].x * triangle[1].y * triangle[2].z;

        return (1.0 / 6.0) * (-v321 + v231 + v312 - v132 - v213 + v123);
    },


    // from https://github.com/johannesboyne/node-stl/blob/master/index.js
    finalize() {
        const volumeTotal = Math.abs(this.volume) / 1000;

        this.xCenter /= this.volume;
        this.yCenter /= this.volume;
        this.zCenter /= this.volume;

        const data = {
            volume: volumeTotal, // cubic cm
            weight: volumeTotal * this.density, // gm
            boundingBox: [
                this.maxx - this.minx,
                this.maxy - this.miny,
                this.maxz - this.minz
            ],
            area: this.area,
            centerOfMass: [this.xCenter, this.yCenter, this.zCenter]
        };

        this.measuredData = data;
    },

    parseBinary: function ( data ) {

        var reader = new DataView( data );
        var faces = reader.getUint32( 80, true );

        var r, g, b, hasColors = false, colors;
        var defaultR, defaultG, defaultB, alpha;

        // process STL header
        // check for default color in header ("COLOR=rgba" sequence).

        for ( var index = 0; index < 80 - 10; index++ ) {

            if ( ( reader.getUint32( index, false ) == 0x434F4C4F /* COLO*/ ) &&
                ( reader.getUint8( index + 4 ) == 0x52 /* 'R'*/ ) &&
                ( reader.getUint8( index + 5 ) == 0x3D /* '='*/ ) ) {

                hasColors = true;
                colors = [];

                defaultR = reader.getUint8( index + 6 ) / 255;
                defaultG = reader.getUint8( index + 7 ) / 255;
                defaultB = reader.getUint8( index + 8 ) / 255;
                alpha = reader.getUint8( index + 9 ) / 255;

            }

        }

        var dataOffset = 84;
        var faceLength = 12 * 4 + 2;

        var geometry = new THREE.BufferGeometry();

        var vertices = [];
        var normals = [];

        for ( var face = 0; face < faces; face++ ) {

            var start = dataOffset + face * faceLength;
            var normalX = reader.getFloat32( start, true );
            var normalY = reader.getFloat32( start + 4, true );
            var normalZ = reader.getFloat32( start + 8, true );

            if ( hasColors ) {

                var packedColor = reader.getUint16( start + 48, true );

                if ( ( packedColor & 0x8000 ) === 0 ) {

                    // facet has its own unique color

                    r = ( packedColor & 0x1F ) / 31;
                    g = ( ( packedColor >> 5 ) & 0x1F ) / 31;
                    b = ( ( packedColor >> 10 ) & 0x1F ) / 31;

                } else {

                    r = defaultR;
                    g = defaultG;
                    b = defaultB;

                }

            }

            let triangle = new Array(3);

            for ( var i = 1; i <= 3; i++ ) {

                var vertexstart = start + i * 12;

                const data = [
                    reader.getFloat32( vertexstart, true ),
                    reader.getFloat32( vertexstart + 4, true ),
                    reader.getFloat32( vertexstart + 8, true )
                ];

                vertices.push( data[0] );
                vertices.push( data[1] );
                vertices.push( data[2] );

                triangle[i - 1] = new Vector3(
                    data[0],
                    data[1],
                    data[2]
                );

                normals.push( normalX, normalY, normalZ );

                if ( hasColors ) {

                    colors.push( r, g, b );

                }

            }

            this.addTriangle(triangle);

        }

        this.finalize();

        geometry.addAttribute( 'position', new THREE.BufferAttribute( new Float32Array( vertices ), 3 ) );
        geometry.addAttribute( 'normal', new THREE.BufferAttribute( new Float32Array( normals ), 3 ) );

        if ( hasColors ) {

            geometry.addAttribute( 'color', new THREE.BufferAttribute( new Float32Array( colors ), 3 ) );
            geometry.hasColors = true;
            geometry.alpha = alpha;

        }

        return geometry;

    },

    parseASCII: function ( data ) {

        var geometry, length, patternFace, patternNormal, patternVertex, result, text;
        geometry = new THREE.BufferGeometry();
        patternFace = /facet([\s\S]*?)endfacet/g;

        var vertices = [];
        var normals = [];

        var normal = new THREE.Vector3();

        while ( ( result = patternFace.exec( data ) ) !== null ) {

            text = result[ 0 ];
            patternNormal = /normal[\s]+([\-+]?[0-9]+\.?[0-9]*([eE][\-+]?[0-9]+)?)+[\s]+([\-+]?[0-9]*\.?[0-9]+([eE][\-+]?[0-9]+)?)+[\s]+([\-+]?[0-9]*\.?[0-9]+([eE][\-+]?[0-9]+)?)+/g;
            while ( ( result = patternNormal.exec( text ) ) !== null ) {

                normal.x = parseFloat( result[ 1 ] );
                normal.y = parseFloat( result[ 3 ] );
                normal.z = parseFloat( result[ 5 ] );

            }

            patternVertex = /vertex[\s]+([\-+]?[0-9]+\.?[0-9]*([eE][\-+]?[0-9]+)?)+[\s]+([\-+]?[0-9]*\.?[0-9]+([eE][\-+]?[0-9]+)?)+[\s]+([\-+]?[0-9]*\.?[0-9]+([eE][\-+]?[0-9]+)?)+/g;

            const triangle = [];

            while ( ( result = patternVertex.exec( text ) ) !== null ) {

                const data = [
                    parseFloat( result[ 1 ] ),
                    parseFloat( result[ 3 ] ),
                    parseFloat( result[ 5 ] )
                ];

                vertices.push( data[0], data[1], data[2] );

                triangle.push(new Vector3(data[0], data[1], data[2]));

                normals.push( normal.x, normal.y, normal.z );

            }

            this.addTriangle(triangle);

        }

        this.finalize();

        geometry.addAttribute( 'position', new THREE.BufferAttribute( new Float32Array( vertices ), 3 ) );
        geometry.addAttribute( 'normal', new THREE.BufferAttribute( new Float32Array( normals ), 3 ) );

        return geometry;

    },

    ensureString: function ( buf ) {

        if ( typeof buf !== 'string' ) {

            var array_buffer = new Uint8Array( buf );
            var strArray = [];
            for ( var i = 0; i < buf.byteLength; i++ ) {

                strArray.push( String.fromCharCode( array_buffer[ i ] ) ); // implicitly assumes little-endian

            }
            return strArray.join( '' );

        } else {

            return buf;

        }

    },

    ensureBinary: function ( buf ) {

        if ( typeof buf === 'string' ) {

            var array_buffer = new Uint8Array( buf.length );
            for ( var i = 0; i < buf.length; i++ ) {

                array_buffer[ i ] = buf.charCodeAt( i ) & 0xff; // implicitly assumes little-endian

            }
            return array_buffer.buffer || array_buffer;

        } else {

            return buf;

        }

    }

};

export { STLLoader };
